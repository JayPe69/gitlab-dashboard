import { LabelSerie, ProjectsTimeSerie } from 'Type';
import interpolate from 'color-interpolate';

export default class ColorHelper {
    static getColors(series:Array<LabelSerie | ProjectsTimeSerie>, colors: Array<string>): Array<string> {
        let colorIndex = 0;
        const colorMap = interpolate(colors);
        const increment = Number(Number(1 / (series.length - 1)).toFixed(2));
        const colorsTab: Array<string> = series.map(() => {
            const result = colorMap(colorIndex);
            colorIndex += increment;
            return result;
        });
        return colorsTab;
    }
}
