export default class DateHelper {
    static diff(d1: Date, d2: Date): number {
        const d1Day = d1.getTime() / 86400000;
        const d2Day = d2.getTime() / 86400000;
        return Number(d1Day - d2Day);
    }

    static isBetween(startDate: Date, endDate: Date, date: Date): boolean {
        if (this.diff(startDate, date) <= 0 && this.diff(date, endDate) <= 0) {
            return true;
        }
        return false;
    }

    static getSprintDuration(startDate: Date, endDate: Date): number {
        const days = DateHelper.diff(endDate, startDate) + 1;
        const weekends = Math.trunc(Number(days / 7));
        return days - (weekends * 2);
    }

    static getRemainingDays(startDate: Date, endDate: Date, sprintDuration: number): number {
        const now = new Date();
        const diff = DateHelper.getSprintDuration(now, endDate);

        if (DateHelper.diff(startDate, now) >= 0) {
            return sprintDuration;
        }
        if (diff < 0) {
            return 0;
        }

        return Number((diff).toFixed(1));
    }

    static isWeekend(date: Date): boolean {
        return date.getDay() % 6 === 0;
    }

    static getDatesBetween(startDate: Date, endDate: Date): Array<Date> {
        const dates = [];
        const currentDate = startDate;
        while (currentDate < endDate) {
            if (!this.isWeekend(currentDate)) {
                dates.push(new Date(currentDate));
            }
            currentDate.setDate(currentDate.getDate() + 1);
        }
        dates.push(endDate);
        return dates;
    }

    static getDayNumber(startDate: Date, endDate: Date, date:Date): number {
        const currentDate = new Date(startDate.setHours(0, 0, 0, 0));
        let day = -1;
        let index = 1;
        const tmpDate = new Date(date.setHours(0, 0, 0, 0));
        while (currentDate <= endDate && day === -1) {
            if (!this.isWeekend(currentDate)) {
                const diff = this.diff(currentDate, tmpDate);
                if (diff >= 0 && diff < 1) {
                    day = index;
                }
                index++;
            }
            currentDate.setDate(currentDate.getDate() + 1);
        }
        return day;
    }
}
