import { Ticket } from 'Type';

export default class TicketHelper {
    static getEstimateTime(tickets: Array<Ticket>) : number {
        if (tickets.length > 0) {
            const time = tickets.map((ticket) => ticket.time_stats.time_estimate).reduce((acc, item) => acc + item);
            return Number((time / 3600 / 8).toFixed(1));
        }
        return 0;
    }

    static getSpentTime(tickets: Array<Ticket>) : number {
        if (tickets.length > 0) {
            const time = tickets.map((ticket) => ticket.time_stats.total_time_spent).reduce((acc, item) => acc + item);
            return Number((time / 3600 / 8).toFixed(1));
        }
        return 0;
    }

    static capitalizeWord(str: string): string {
        return str.replace(/(^\w|\s\w)(\S*)/g, (_, m1, m2) => m1.toUpperCase() + m2.toLowerCase());
    }

    static getProjectName(ticket: Ticket): string {
        let project = ticket.web_url.split('/-/issues')[0];
        const regex = /-|_/g;
        project = project.substring(project.lastIndexOf('/') + 1).replaceAll(regex, ' ');
        project = this.capitalizeWord(project);
        return project;
    }
}
