export type SubmitFormFunc = (urlForm: string, tokenForm: string, projectForm: string, milestoneForm: string, labelFilterForm: string) => void;
export type Milestone = {
    created_at: Date,
    description: string,
    due_date: Date,
    expired: boolean,
    group_id: number,
    id: number,
    iid: number,
    start_date: Date,
    state: string,
    title: string,
    updated_at: Date,
    web_url: string
};

export type Ticket = {
    id: number,
    title: string,
    created_at: Date,
    closed_at: Date,
    time_stats: TimeStats,
    milestone: Milestone,
    state: string,
    issue_type: string,
    labels: Array<string>,
    web_url: string
};

export type TimeStats = {
    human_time_estimate: string,
    human_total_time_spent: string,
    time_estimate: number,
    total_time_spent: number
};

export type MilestoneInfo = {
    milestone: Milestone | undefined,
    ticketsDuringSprint: Array<Ticket>
};

export type LabelSerie = {
    name: string,
    data: Array<number>
};

export type ProjectsTimeSerie = {
    name: string,
    y: number,
    z: number
};
