import TicketHelper from 'helpers/Tickets';
import React, { MouseEvent } from 'react';
import { Ticket } from 'Type';
import './TicketListing.css';

export interface TicketListingProps {
    tickets: Array<Ticket>,
    listingDisplay: boolean,
    handleDisplay: React.Dispatch<boolean>
}

const TicketListing: React.FC<TicketListingProps> = (props) => {
    const handleCloseClick = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        props.handleDisplay(false);
    };

    return (
        <div id="ticket-layout">
            <div id="tickets">
                <button type="button" onClick={handleCloseClick}>X</button>
                <div id="listing">
                    {props.tickets.map((ticket) => {
                        const project = TicketHelper.getProjectName(ticket);
                        return (
                            <a key={ticket.id} href={ticket.web_url} target="_blank" rel="noreferrer">
                                {project}
                                &nbsp;-&nbsp;
                                #
                                {ticket.id}
                                &nbsp;-&nbsp;
                                {ticket.title}
                            </a>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};

export default TicketListing;
