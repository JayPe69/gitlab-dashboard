import React, { useRef, useState } from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { LabelSerie, Ticket } from 'Type';
import ColorHelper from 'helpers/Colors';
import './Labels.css';

export interface LabelsProps {
    tickets: Array<Ticket>,
    labels: string,
}

const Labels: React.FC<LabelsProps> = (props) => {
    const chartComponentRef = useRef<HighchartsReact.RefObject>(null);
    const labels = props.labels.split(',');
    const series = buildSeriesAccordingToLabels();
    const colors = ColorHelper.getColors(series, ['#292961', '#FFA041', '#ff7800']);

    const [chartOptions] = useState({
        chart: {
            type: 'column'
        },
        colors,
        title: {
            text: ''
        },
        xAxis: {
            categories: [''],
            title: {
                text: null
            },
            labels: {
                overflow: 'justify'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' tickets'
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    style: {
                        color: '#292961',
                        textOutline: 'none',
                        fontSize: '1rem',
                        fontFamily: 'Arial'
                    }
                }
            }
        },
        credits: {
            enabled: true
        },
        series
    });

    function buildSeriesAccordingToLabels(): Array<LabelSerie> {
        const labelSeries = labels.map((label) => {
            const ticketsbyLabel = props.tickets.filter(ticket => ticket.labels.includes(label.trim()));
            const labelSerie: LabelSerie = {
                name: label,
                data: [ticketsbyLabel.length]
            };
            return labelSerie;
        });
        return labelSeries.filter((labelSerie) => labelSerie.data[0] > 0);
    }

    return (
        <div className="labels">
            <h2>Labels</h2>
            <HighchartsReact
              highcharts={Highcharts}
              options={chartOptions}
              ref={chartComponentRef}
            />
        </div>
    );
};
export default Labels;
