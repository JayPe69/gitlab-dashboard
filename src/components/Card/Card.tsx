import TicketListing from 'components/TicketListing/TicketListing';
import TicketHelper from 'helpers/Tickets';
import React, { MouseEvent, useState } from 'react';
import { Ticket } from 'Type';
import './Card.css';

export interface CardProps {
    total: Array<Ticket>,
    open: Array<Ticket>,
    close: Array<Ticket>,
    title: string
}

const Card: React.FC<CardProps> = (props) => {
    const openSpentTime = TicketHelper.getSpentTime(props.open);
    const closeSpentTime = TicketHelper.getSpentTime(props.close);
    const openEstimateTime = TicketHelper.getEstimateTime(props.open);
    const closeEstimateTime = TicketHelper.getEstimateTime(props.close);
    const [tickets, setTickets] = useState(Array<Ticket>);
    const [listingDisplay, setListingDisplay] = useState(false);

    const handleOpenClick = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        setTickets(props.open);
        setListingDisplay(true);
    };

    const handleCloseClick = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        setTickets(props.close);
        setListingDisplay(true);
    };

    return (
        <>
            {(props.total.length > 0) && (
            <div className="card">
                <h2 title={props.title}>{props.title}</h2>
                <div className="total">
                    <h3>Total</h3>
                    <div className="number">{props.total.length}</div>
                    <div className="numbers">
                        <button type="button" className="open" onClick={handleOpenClick}>
                            <h4>Open</h4>
                            <div className="number">{props.open.length}</div>
                        </button>
                        <button type="button" className="close" onClick={handleCloseClick}>
                            <h4>Closed</h4>
                            <div className="number">{props.close.length}</div>
                        </button>
                    </div>
                    <h3>Percentages</h3>
                    <div className="numbers">
                        <div className="open">
                            <div className="number">
                                {Math.round((props.open.length * 100) / props.total.length)}
                                <span className="percent">%</span>
                            </div>
                        </div>
                        <div className="close">
                            <div className="number">
                                {Math.round((props.close.length * 100) / props.total.length)}
                                <span className="percent">%</span>
                            </div>
                        </div>
                    </div>
                    <h3>Estimate time</h3>
                    <div className="numbers">
                        <div className="open">
                            <div className="number">
                                {openEstimateTime}
                                <span className="percent">d</span>
                            </div>
                        </div>
                        <div className="close">
                            <div className="number">
                                {closeEstimateTime}
                                <span className="percent">d</span>
                            </div>
                        </div>
                    </div>
                    <h3>Spent time</h3>
                    <div className="numbers">
                        <div className="open">
                            <div className="number">
                                {openSpentTime}
                                <span className="percent">d</span>
                            </div>
                        </div>
                        <div className="close">
                            <div className="number">
                                {closeSpentTime}
                                <span className="percent">d</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            )}
            {listingDisplay && (
            <TicketListing tickets={tickets} listingDisplay={listingDisplay} handleDisplay={setListingDisplay} />
            )}
        </>
    );
};

export default Card;
