import 'react-calendar/dist/Calendar.css';
import './Milestone.css';
import React from 'react';
import { Milestone as MilestoneType } from 'Type';
import Calendar from 'react-calendar';
import DateHelper from 'helpers/Date';

export interface MilestoneProps {
    milestone: MilestoneType
}

const Milestone: React.FC<MilestoneProps> = (props) => {
    const startDate = new Date(props.milestone.start_date);
    const dueDate = new Date(props.milestone.due_date);

    const sprintDuration = DateHelper.getSprintDuration(startDate, dueDate);
    const sprintRemainingDays = DateHelper.getRemainingDays(startDate, dueDate, sprintDuration);

    return (
        <div id="milestone">
            <h2><a href={props.milestone.web_url} target="_blank" rel="noreferrer">{props.milestone.title}</a></h2>
            <span className="start-date">{props.milestone.start_date.toString()}</span>
            <span className="due-date">{props.milestone.due_date.toString()}</span>
            <Calendar
              defaultValue={[startDate, dueDate]}
              minDate={startDate}
              maxDate={dueDate}
              maxDetail="month"
              minDetail="month"
            />
            <div className="numbers">
                <div className="open">
                    <h4>Duration</h4>
                    <div className="number">
                        {sprintDuration}
                        <span className="percent">d</span>
                    </div>
                </div>
                <div className="close">
                    <h4>Remaining</h4>
                    <div className="number">
                        {sprintRemainingDays}
                        <span className="percent">d</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Milestone;
