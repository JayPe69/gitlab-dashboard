import React, {
    FormEvent, useState
} from 'react';
import { SubmitFormFunc } from 'Type';
import { useCookies } from 'react-cookie';
import './HeaderForm.css';

export interface HeaderFormProps {
    submitForm: SubmitFormFunc,
    handleCapacity: React.Dispatch<number>,
    handleLabels: React.Dispatch<string>,
    devCapacity: number,
    labels: string
}

const HeaderForm: React.FC<HeaderFormProps> = (props) => {
    const [cookies, setCookie, removeCookie] = useCookies();
    const [url, setUrl] = useState(cookies.url || '');
    const [token, setToken] = useState(cookies.token || '');
    const [project, setProject] = useState(cookies.project || '');
    const [milestone, setMilestone] = useState(cookies.milestone || '');
    const [labelFilter, setLabelFilter] = useState(cookies.labelFilter || '');
    const [keep, setKeep] = useState(cookies.keep === 'true');

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        // @TODO: Check fields format and values submitted to avoid errors : Manage errors
        if (keep) {
            const date = new Date();
            date.setDate(date.getDate() + 7);

            setCookie('url', url, { path: '/', expires: date });
            setCookie('token', token.trim(), { path: '/', expires: date });
            setCookie('milestone', milestone.trim(), { path: '/', expires: date });
            setCookie('labelFilter', labelFilter.trim(), { path: '/', expires: date });
            setCookie('project', project.trim(), { path: '/', expires: date });
            setCookie('keep', keep, { path: '/', expires: date });
            setCookie('devCapacity', props.devCapacity, { path: '/', expires: date });
            setCookie('labels', props.labels.trim(), { path: '/', expires: date });
        } else {
            removeCookie('url');
            removeCookie('token');
            removeCookie('milestone');
            removeCookie('labelFilter');
            removeCookie('project');
            removeCookie('keep');
            removeCookie('devCapacity');
            removeCookie('labels');
        }

        props.submitForm(`${url}/api/v4`, token.trim(), project.trim(), milestone.trim(), labelFilter.trim());
    };

    const handleTokenChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        setToken(evtTarget.value);
    };

    const handleUrlChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        setUrl(evtTarget.value);
    };

    const handleMilestoneChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        setMilestone(evtTarget.value);
    };

    const handleLabelFilterChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        setLabelFilter(evtTarget.value);
    };

    const handleProjectChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        setProject(evtTarget.value);
    };

    const handleDevCapacityChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        props.handleCapacity(Number(evtTarget.value));
    };

    const handleLabelsChange = (event: FormEvent<HTMLInputElement>) => {
        const evtTarget = (event.target as HTMLInputElement);
        props.handleLabels(evtTarget.value);
    };

    const handleKeepChange = () => {
        setKeep(!keep);
    };

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="url">
                <span>Gitlab URL *</span>
                <input id="url" type="text" placeholder="Gitlab URL" value={url} onChange={handleUrlChange} />
            </label>
            <label htmlFor="token">
                <span>Token *</span>
                <input id="token" type="password" placeholder="Personal Access Token" value={token} onChange={handleTokenChange} />
            </label>
            <label htmlFor="milestone">
                <span>Milestone *</span>
                <input id="milestone" type="text" placeholder="Milestone" value={milestone} onChange={handleMilestoneChange} />
            </label>
            <label htmlFor="label-filter">
                <span>Label Filter</span>
                <input id="label-filter" type="text" placeholder="Label Filter" value={labelFilter} onChange={handleLabelFilterChange} />
            </label>
            <label htmlFor="project">
                <span>Project</span>
                <input id="project" type="text" placeholder="Project" value={project} onChange={handleProjectChange} />
            </label>
            <label htmlFor="labels">
                <span>Labels</span>
                <input id="labels" type="text" placeholder="Label 1, Label 2" value={props.labels} onChange={handleLabelsChange} />
            </label>
            <label htmlFor="capacity">
                <span>Dev Capacity *</span>
                <input id="capacity" type="number" placeholder="Dev Capacity" min="0" max="1000" value={props.devCapacity} onChange={handleDevCapacityChange} />
            </label>
            <input type="submit" value="Submit" />
            <label htmlFor="keep">
                Keep data (not secured) :
                <input id="keep" type="checkbox" checked={keep} onChange={handleKeepChange} />
            </label>
        </form>
    );
};

export default HeaderForm;
