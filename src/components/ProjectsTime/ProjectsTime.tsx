import React, { useRef, useState } from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import variablePie from 'highcharts/modules/variable-pie.js';
import { ProjectsTimeSerie, Ticket } from 'Type';
import ColorHelper from 'helpers/Colors';
import './ProjectsTime.css';
import TicketHelper from 'helpers/Tickets';

export interface ProjectsTimeProps {
    tickets: Array<Ticket>
}

const ProjectsTime: React.FC<ProjectsTimeProps> = (props) => {
    const chartComponentRef = useRef<HighchartsReact.RefObject>(null);
    variablePie(Highcharts);
    const series = getProjectsTimeSeries();
    const colors = ColorHelper.getColors(series, ['#ff7800', '#FFA041', '#292961']);
    const [chartOptions] = useState({
        chart: {
            type: 'variablepie'
        },
        colors,
        title: {
            text: ''
        },
        plotOptions: {
            variablepie: {
                dataLabels: {
                    style: {
                        color: 'black',
                        textOutline: 'none',
                        fontSize: '0.80rem',
                        fontFamily: 'Arial',
                        textShadow: '0px 0px 2px white'
                    }
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left'
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<span style="color:{point.color};text-transform:capitalize;">\u25CF</span> <b> {point.name}</b><br/>'
                + 'Estimate: <b>{point.y} days</b><br/>'
                + 'Spent: <b>{point.z} days</b><br/>'
        },
        series: [{
            minPointSize: 10,
            innerSize: '20%',
            zMin: 0,
            name: 'Projects',
            data: series
        }]
    });

    function getProjectsTimeSeries(): Array<ProjectsTimeSerie> {
        const projectsTimeSeries: Array<ProjectsTimeSerie> = [];
        props.tickets.forEach((ticket) => {
            const project = TicketHelper.getProjectName(ticket);
            const index = projectsTimeSeries.findIndex(pts => pts.name === project);
            if (index > -1) {
                projectsTimeSeries[index].y += ticket.time_stats.time_estimate;
                projectsTimeSeries[index].z += ticket.time_stats.total_time_spent;
            } else if (ticket.time_stats.time_estimate !== 0 || ticket.time_stats.total_time_spent !== 0) {
                projectsTimeSeries.push({
                    name: project,
                    y: ticket.time_stats.time_estimate,
                    z: ticket.time_stats.total_time_spent
                });
            }
        });

        const results = projectsTimeSeries.map(pts => {
            pts.y = Number((pts.y / 3600 / 8).toFixed(1));
            pts.z = Number((pts.z / 3600 / 8).toFixed(1));
            return pts;
        }).sort((r1, r2): number => {
            if (r1.y > r2.y) {
                return 1;
            }
            return -1;
        });

        return (results);
    }

    return (
        <div className="projects-time">
            <h2>Times by project</h2>
            <HighchartsReact
              highcharts={Highcharts}
              options={chartOptions}
              ref={chartComponentRef}
            />
        </div>
    );
};

export default ProjectsTime;
