import React, { useRef, useState } from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { Milestone, Ticket } from 'Type';
import DateHelper from 'helpers/Date';
import './Burndown.css';

export interface BurndownProps {
    milestone: Milestone,
    tickets: Array<Ticket>,
}

const Burndown: React.FC<BurndownProps> = (props) => {
    const chartComponentRef = useRef<HighchartsReact.RefObject>(null);
    const startDate = new Date(props.milestone.start_date);
    const dueDate = new Date(props.milestone.due_date);
    const sprintDuration = DateHelper.getSprintDuration(startDate, dueDate);
    // Need new date objects as we don't want to modifiy startDate and dueDate for other process
    const dayNumber = DateHelper.getDayNumber(new Date(props.milestone.start_date), new Date(props.milestone.due_date), new Date());
    const totalTickets = props.tickets.filter((ticket) => {
        const result = DateHelper.diff(new Date(props.milestone.start_date), new Date(ticket.created_at));
        if (result > 0) {
            return true;
        }
        return false;
    });

    // const totalTickets = props.tickets;

    const [chartOptions] = useState({
        chart: {
            type: 'line',
            height: '400px'
        },
        colors: ['#FFA041', '#ff7800'],
        title: {
            text: ''
        },
        plotOptions: {
            line: {
                lineWidth: 3
            },
            tooltip: {
                hideDelay: 200
            }
        },
        xAxis: {
            categories: getXAxisCategories(),
            plotBands: [{
                from: dayNumber - 0.5,
                to: dayNumber + 0.5,
                color: '#ff780024'
            }]
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1
            }]
        },
        tooltip: {
            valueSuffix: ' tickets',
            crosshairs: true,
            shared: true
        },
        legend: {
            layout: 'horizontal',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: [{
            name: 'Ideal Burn',
            color: '#2929618c',
            lineWidth: 2,
            data: getIdealBurnSeries()
        }, {
            name: 'Actual Burn',
            color: '#ff7800',
            marker: {
                radius: 6
            },
            data: getActualBurnSeries()
        }]
    });

    function getXAxisCategories() : Array<string> {
        const categories: Array<string> = [];
        categories.push('Day 0');
        for (let index = 1; index < sprintDuration + 1; index++) {
            categories.push(`Day ${ index}`);
        }
        return categories;
    }

    function getIdealBurnSeries() : Array<number> {
        const average = Number((totalTickets.length / (sprintDuration)).toPrecision(3));
        const idealSeries: Array<number> = [];
        let data = totalTickets.length;
        for (let index = 0; index < sprintDuration; index++) {
            idealSeries.push(Number(data.toPrecision(3)));
            data -= average;
        }
        idealSeries.push(0);
        return idealSeries;
    }

    function getActualBurnSeries() : Array<number> {
        const dates = DateHelper.getDatesBetween(new Date(props.milestone.start_date), new Date(props.milestone.due_date));

        const actualSeries = dates.map(date => {
            const ticketsClose = props.tickets.filter((ticket) => {
                if (ticket.state === 'closed') {
                    const closeDate = new Date(ticket.closed_at).setHours(0, 0, 0, 0);
                    const result = DateHelper.diff(new Date(closeDate), date);
                    if (result <= 0) {
                        return true;
                    }
                }
                return false;
            });
            const ticketsOpened = props.tickets.filter((ticket) => {
                const createdDate = new Date(ticket.created_at).setHours(0, 0, 0, 0);
                const result = DateHelper.diff(new Date(createdDate), date);
                if (result <= 0) {
                    return true;
                }
                return false;
            });
            return ticketsOpened.length - ticketsClose.length;
        });

        actualSeries.unshift(totalTickets.length);
        return actualSeries;
    }

    return (
        <div className="burndown">
            <h2>Burndown Chart</h2>
            <HighchartsReact
              highcharts={Highcharts}
              options={chartOptions}
              ref={chartComponentRef}
            />
        </div>
    );
};

export default Burndown;
