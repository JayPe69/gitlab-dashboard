import React, { useRef, useState } from 'react';
import { Milestone, Ticket } from 'Type';
import DateHelper from 'helpers/Date';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import TicketHelper from 'helpers/Tickets';
import './SprintTime.css';

export interface SprintTimeProps {
    milestone: Milestone,
    tickets: Array<Ticket>,
    devCapacity: number
}

const SprintTime: React.FC<SprintTimeProps> = (props) => {
    function getRemainingCapacity() {
        const dayCapacity = Number((props.devCapacity / sprintDuration).toFixed(1));
        return Number((dayCapacity * sprintRemainingDays).toFixed(1));
    }

    const estimateTime = TicketHelper.getEstimateTime(props.tickets);
    const spentTime = TicketHelper.getSpentTime(props.tickets);
    const remainingTime = Number((estimateTime - spentTime).toFixed(1));
    const startDate = new Date(props.milestone.start_date);
    const dueDate = new Date(props.milestone.due_date);

    const sprintDuration = DateHelper.getSprintDuration(startDate, dueDate);
    const sprintRemainingDays = DateHelper.getRemainingDays(startDate, dueDate, sprintDuration);
    const remainingCapacity = getRemainingCapacity();
    const doComplete = (remainingCapacity - remainingTime) > 0;
    const chartComponentRef = useRef<HighchartsReact.RefObject>(null);
    const [chartOptions] = useState({
        chart: {
            type: 'column',
            height: '250px'
        },
        colors: ['#FFA041', '#ff7800'],
        title: {
            text: ''
        },
        xAxis: {
            categories: [''],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' days'
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    style: {
                        color: '#292961',
                        textOutline: 'none',
                        fontSize: '1rem',
                        fontFamily: 'Arial'
                    }
                }
            }
        },
        credits: {
            enabled: true
        },
        series: [{
            name: 'Estimate',
            data: [estimateTime]
        }, {
            name: 'Spent',
            data: [spentTime]
        }]
    });

    return (
        <>
            {(estimateTime || spentTime) && (
            <div className="sprint-time">
                <h2>Times</h2>
                <HighchartsReact
                  highcharts={Highcharts}
                  options={chartOptions}
                  ref={chartComponentRef}
                />
                <div className="numbers">
                    <div className="open">
                        <h4 title="Time To Complete">TTC</h4>
                        <div className="number">
                            {remainingTime}
                            <span className="percent">d</span>
                        </div>
                    </div>
                    <div className="close">
                        <h4 title="Dev capacity">Capacity</h4>
                        <div className="number">
                            {remainingCapacity}
                            <span className="percent">d</span>
                        </div>
                    </div>
                </div>
                <div className="inTime">
                    {
            doComplete ? (

                <div className="number">
                    In Time
                </div>
            ) : (
                <div className="number">
                    Not In Time
                </div>
            )
            }

                </div>
            </div>
            )}
        </>
    );
};

export default SprintTime;
