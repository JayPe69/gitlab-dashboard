import React from 'react';
import './Loading.css';

const Loading: React.FC<Record<string, never>> = () => (
    <div id="loading">
        <div id="loader">
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
        </div>
    </div>
);

export default Loading;
