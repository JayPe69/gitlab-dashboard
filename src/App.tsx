import axios, { AxiosRequestConfig } from 'axios';
import React, { useState, useRef } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { Milestone as MilestoneType, MilestoneInfo, Ticket } from 'Type';
import Loading from 'components/Loading/Loading';
import Milestone from 'components/Milestone/Milestone';
import Card from 'components/Card/Card';
import HeaderForm from 'components/HeaderForm/HeaderForm';
import { useCookies } from 'react-cookie';
import SprintTime from 'components/SprintTime/SprintTime';
import DateHelper from 'helpers/Date';
import Burndown from 'components/Burndown/Burndown';
import Labels from 'components/Labels/Labels';
import ProjectsTime from 'components/ProjectsTime/ProjectsTime';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';

const App: React.FC = () => {
    const [cookies] = useCookies();
    const [tickets, setTickets] = useState(Array<Ticket>);
    const [loading, setLoading] = useState(false);
    const [devCapacity, setDevCapacity] = useState(cookies.devCapacity || 10);
    const [labels, setLabels] = useState(cookies.labels || '');
    const isMilestone = useRef<boolean>(false);

    async function handleSubmitForm(urlForm: string, tokenForm: string, projectForm: string, milestoneForm: string, labelFilterForm: string) : Promise<void> {
        setLoading(true);
        // @TODO: check for gitlab URL is a URL
        // @TODO: avoid bad characters
        const paramsArray = [['scope', 'all'], ['per_page', '100']];
        if (milestoneForm) {
            paramsArray.push(['milestone', milestoneForm]);
            isMilestone.current = true;
        }
        if (projectForm) {
            urlForm = `${urlForm}/projects/${projectForm}`;
        }
        const params = new URLSearchParams(paramsArray);
        const config = { headers: { 'Private-Token': tokenForm }, params };
        urlForm = `https://${urlForm}/issues`;

        try {
            let data = await getRecursiveIssues(urlForm, config);
            toast.success('I got back the data');
            if (labelFilterForm) {
                data = data.filter(ticket => ticket.labels.includes(labelFilterForm.trim()));
            }
            setTickets(data);
            setLoading(false);
        } catch (error) {
            toast.error('Error ! Check the console log');
            console.error(error);
            setLoading(false);
        }
    }

    async function getRecursiveIssues(url: string, config: AxiosRequestConfig) : Promise<Array<Ticket>> {
        let data: Array<Ticket> = [];
        const response = await axios.get(url, config);
        // eslint-disable-next-line no-console
        if (response.headers['x-next-page'] !== undefined && response.headers['x-next-page']) {
            let nextLink = response.headers.link.split(',').filter(item => item.includes('next'))[0];
            nextLink = nextLink.substring(nextLink.indexOf('<') + 1, nextLink.indexOf('>'));
            data = await getRecursiveIssues(nextLink, config);
        }
        return data.concat(response.data);
    }

    function getMilestoneInfo(ticketsParam: Array<Ticket>) : MilestoneInfo {
        if (ticketsParam.length > 0) {
            const milestoneTmp = ticketsParam[0].milestone as MilestoneType;
            const ticketsTmp = ticketsParam.filter(ticket => DateHelper.isBetween(new Date(milestoneTmp.start_date), new Date(milestoneTmp.due_date), new Date(ticket.created_at)));
            return ({
                milestone: milestoneTmp,
                ticketsDuringSprint: ticketsTmp
            });
        }
        return ({
            milestone: undefined,
            ticketsDuringSprint: []
        });
    }

    const ticketsOpen = tickets.filter(ticket => ticket.state === 'opened');
    const ticketsClose = tickets.filter(ticket => ticket.state === 'closed');

    const issues = tickets.filter(ticket => ticket.issue_type === 'issue');
    const issuesOpen = issues.filter(ticket => ticket.state === 'opened');
    const issuesClose = issues.filter(ticket => ticket.state === 'closed');

    const incidents = tickets.filter(ticket => ticket.issue_type === 'incident');
    const incidentsOpen = incidents.filter(ticket => ticket.state === 'opened');
    const incidentsClose = incidents.filter(ticket => ticket.state === 'closed');

    const { milestone, ticketsDuringSprint } = getMilestoneInfo(tickets);
    const ticketsDuringSprintOpen = ticketsDuringSprint.filter(ticket => ticket.state === 'opened');
    const ticketsDuringSprintClose = ticketsDuringSprint.filter(ticket => ticket.state === 'closed');

    // @TODO: Make the app scrolling in the stats div and not in body

    return (
        <div id="app">
            <header>
                <HeaderForm
                  submitForm={handleSubmitForm}
                  handleCapacity={setDevCapacity}
                  handleLabels={setLabels}
                  devCapacity={devCapacity}
                  labels={labels}
                />
            </header>
            {
            loading ? (
                <Loading />
            ) : tickets.length > 0 && (
                <section id="stats">
                    { milestone !== undefined && (
                        <>
                            <Milestone milestone={milestone} />
                            <SprintTime milestone={milestone} tickets={tickets} devCapacity={devCapacity} />
                            <Burndown milestone={milestone} tickets={tickets} />
                            <Labels tickets={tickets} labels={labels} />
                            <ProjectsTime tickets={tickets} />
                            <Card title="Tickets" total={tickets} open={ticketsOpen} close={ticketsClose} />
                            <Card title="Added during the Sprint" total={ticketsDuringSprint} open={ticketsDuringSprintOpen} close={ticketsDuringSprintClose} />
                        </>
                    )}
                    { milestone === undefined && (
                        <>
                            <Labels tickets={tickets} labels={labels} />
                            <Card title="Tickets" total={tickets} open={ticketsOpen} close={ticketsClose} />
                        </>
                    )}
                    <Card title="Issues" total={issues} open={issuesOpen} close={issuesClose} />
                    <Card title="Incidents" total={incidents} open={incidentsOpen} close={incidentsClose} />

                </section>
            )
            }
            <ToastContainer
              position="bottom-center"
              autoClose={2500}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
        </div>
    );
};

export default App;
