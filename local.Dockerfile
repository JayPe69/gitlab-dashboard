FROM node:16.17.0-bullseye-slim as dev

ARG GITLAB_DASHBOARD_FRONT_PORT

RUN mkdir -p /var/www/app/gl-dashboard/node_modules \
    && chown -R node:node /var/www/app/gl-dashboard \
    && apt-get update \
    && apt-get -qq -y install curl

WORKDIR /var/www/app/gl-dashboard

COPY --chown=node:node . .

USER node

RUN yarn install

EXPOSE $GITLAB_DASHBOARD_FRONT_PORT

CMD ["yarn", "dev"]