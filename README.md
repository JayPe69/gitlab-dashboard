# Getting Started with Gitlab Dashboard

This project has been created to have an overview of your sprint (milestone) and somes KPIs to help you understand what is going on and make sure your scrum team is doing things correctly.  
This project has been created to follow gitlab free tiers milestone.  
It means for example, that you won't retrieve weight stats, but only estimate/spent time stats.  

# How to use it

## Access it

Please find the [website here](https://jaype69.gitlab.io/gitlab-dashboard/)

## What to do with the form ?

![image-1.png](./readme/form-screen.png)

**Gitlab URL Input**

This field will tell the app to get back data on the specific gitlab URL.

> For example : gitlab.com  
> For example : mycompany.com

**Token Input**

This field will let the app access the gitlab API, as this app is a frontend app, **it does not store** the value on a server or somewhere else.

> For example : You can [create a personnal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the unique role : read_api

**Milestone Input**

In this field, you have to specify the sprint (milestone) name of the sprint you want to follow.  

> For example : Sprint 6

**Label Filter Input**

In this field, you can specify a label to filter all the KPI on this label.  
It will let you the possibility to filter by feature team (front/back/devops) or another kind of label.  

> For example : DevOps

**Project Input**

This field is used to filter the data on a specific project if you have a milestone that embed several projects. It cannot be used without milestone for now.  
You have to specify the "id" of your project.  

> For example : 134

**Labels**

With gitlab, you can create dashboards using labels, and specifying other labels on cards.  
The goal of this field is to specify labels you want to see in the dashboard. Most of the time, you'll specify labels that match your gitlab dashboard.  

> For example : Backlog,To Do,Doing,Done

**Dev Capacity**

Here you have to specify in days the capacity of your dev team.  
If you have 3 developers and your milestone is a 10 days sprint, then the dev capacity will be : 30.  

> For example : 30

**Keep Data (not secured)**

This option will let you the possibility to store the form data in a cookie that will be available for 7 days.  
Using it is very convenient but not secured, as the data is stored in plain text in your browser.  
To avoid too much security breaches, your access token should only have read_api access with an expiration date.  
Storing token as plain text is unsecured if the app has XSS breaches (I didn't test it for now).  

# How to develop

By default, I use docker to develop in local.  
So if docker is already installed, you just need to :  
 - Clone the project
 - Set up 2 Environment Variables :
   - GITLAB_DASHBOARD_FRONT_URL
   - GITLAB_DASHBOARD_FRONT_PORT
 - Build Docker images with 
   ```
   make build
   ```
 - Up docker containers with 
   ```
   make up
   ```
 - Visit your local website and development using
   ```
   make get-urls
   ```

## Ease and make it faster

In the Makefile you'll see two other commands.  

```
make cpnode
```

Will let you the possibility to copy the node_modules folder from your container into your host machine.  
Without doing this, your IDE ,won't understand your code, and won't give you autocomplete and other stuffs.  

```
make cpcache
```

Will let you the possibility to copy the .yarn/cache folder from your container into your host machine.  
Without doing this, your build will still be long, whereas with the Yarn 3 Cache, the build is very fast.  