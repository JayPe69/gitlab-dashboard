help: ## Display this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

get-urls: ## List Urls of the application (make sure you started the project containers)
	@printf 'Front: ${GITLAB_DASHBOARD_FRONT_URL}:${GITLAB_DASHBOARD_FRONT_PORT}\n'

build: ## Build all docker images
	docker compose -f ./local.docker-compose.yml build

up: ## Up all containers
	docker compose -f ./local.docker-compose.yml up

stop: ## Stop all containers
	docker compose -f ./local.docker-compose.yml stop

down: ## Down all containers, stop and remove containers, remove networks
	docker compose -f ./local.docker-compose.yml down

down-v: ## Down all containers, stop and remove containers, remove networks, remove volumes
	docker compose -f ./local.docker-compose.yml down -v

cpnode: ## Copy the node modules folder to your host from the gl-dashboard container (the container has to be up)
	docker cp gl-dashboard:/var/www/app/gl-dashboard/node_modules/ ./

cpcache: ## Copy the yarn cache folder to your host from the gl-dashboard container (the container has to be up)
	docker cp gl-dashboard:/var/www/app/gl-dashboard/.yarn/cache ./.yarn/